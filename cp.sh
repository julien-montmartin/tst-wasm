BB=/home/julien/Dépôts/tst-wasm
GH=/home/julien/Dépôts/ternary-tree-wasm

mkdir -p ${GH}/src
mkdir -p ${GH}/docs

cp ${BB}/Cargo.toml ${GH}/Cargo.toml
cp ${BB}/src/lib.rs ${GH}/src/lib.rs
cp ${BB}/README.md ${GH}/README.md
cp ${BB}/pack.sh ${GH}/pack.sh
cp ${BB}/rust-toolchain ${GH}/rust-toolchain
cp ${BB}/docs/doc.org ${GH}/docs/
cp ${BB}/docs/tree.dot ${GH}/docs/
cp ${BB}/docs/tree.svg ${GH}/docs/
#cp ${BB}/docs/tree.svg ${GH}/docs/
cp -R ${BB}/docs/css ${GH}/docs/
