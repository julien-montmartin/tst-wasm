// Note that a dynamic `import` statement here is required due to
// webpack/webpack#6615, but in theory `import { greet } from './pkg';`
// will work here one day as well!
const rust = import('./pkg');


function check(code, res, expected) {

    if(res!=null && expected!=null) {

        res = res.toString();
        expected = expected.toString();
    }

    let mark = res == expected ? '✔' :  '❌';
    let opt = res == expected ? '' : ' ≠ ' + expected
    let line = mark + ' ' + code + ' → ' + res + opt;

    let main = document.getElementById('main');

    let newp = document.createElement('section');
    newp.textContent=line;

    main.appendChild(newp);
}

function pushSection(pTitle, pComment, pCode, pRes, pExpected) {

    let title = document.createElement('h2');
    title.textContent=pTitle;
    let comment = document.createElement('p');
    comment.textContent=pComment;
    let code = document.createElement('code');
    code.innerText=pCode;

    if(pRes!=null && pExpected!=null) {

        pRes = pRes.toString();
        pExpected = pExpected.toString();
    }

    let mark = pRes == pExpected ? '✔' :  '❌';
    let opt = pRes == pExpected ? '' : ' ≠ ' + pExpected
    let line = mark + ' ' + pRes + opt;

    let result = document.createElement('p');
    result.textContent=line;

    let section = document.createElement('section');
    section.appendChild(title);
    section.appendChild(comment);
    section.appendChild(code);
    section.appendChild(result);

    let main = document.getElementById('main');
    main.appendChild(section);

    return section;
}

function pushMoreCode(pSection, pCode, pRes, pExpected) {

    let code = document.createElement('code');
    code.innerText=pCode;

    if(pRes!=null && pExpected!=null) {

        pRes = pRes.toString();
        pExpected = pExpected.toString();
    }

    let mark = pRes == pExpected ? '✔' :  '❌';
    let opt = pRes == pExpected ? '' : ' ≠ ' + pExpected
    let line = mark + ' ' + pRes + opt;

    let result = document.createElement('p');
    result.textContent=line;

    pSection.appendChild(code);
    pSection.appendChild(result);
}



rust
    .then(module => {

        document.title='Ternary Tree';

        let Tst = module.Tst;

        let title, comment, code, result, expected, lastSection;

        ////////////////////////////////////////////////////////////

        title = 'Create a new tree';
        comment = 'Load the ternary tree module and call new() to create a new empty ternary tree.';
        code = `
        let Tst = module.Tst;
        let t = Tst.new();
        let result = typeof(t);
        `.trim();
        let t = Tst.new();
        result = typeof(t);
        expected = 'object'

        lastSection = pushSection(title, comment, code, result, expected);

        code = `
        result = t.len();
        `.trim();
        result = t.len();
        expected = 0;

        pushMoreCode(lastSection, code, result, expected);

        ////////////////////////////////////////////////////////////

        title = 'Insert and retrieve key-value pairs';
        comment = 'Call insert() to add a key-value pair in the tree. If the key is already present in the tree, its old value is returned. Call get() to retrieve the value associated to a key.'
        code = `
        result = t.insert('abc', 123);
        `.trim();
        result = t.insert('abc', 123);
        expected = null;

        lastSection = pushSection(title, comment, code, result, expected);

        code = `
        result = t.len();
        `.trim();
        result = t.len();
        expected = 1;

        pushMoreCode(lastSection, code, result, expected);

        code = `
        result = t.insert('abc', 321);
        `.trim();
        result = t.insert('abc', 321);
        expected = 123;

        pushMoreCode(lastSection, code, result, expected);

        code = `
        result = t.get('abc');
        `.trim();
        result = t.get('abc');
        expected = 321;

        pushMoreCode(lastSection, code, result, expected);

        code = `
        result = t.get('cba');
        `.trim();
        result = t.get('cba');
        expected = null;

        pushMoreCode(lastSection, code, result, expected);

        ////////////////////////////////////////////////////////////

        res  = t.get('cba');
        check('get("cba")', res, null);

        res = t.insert('abc', 321);
        check('insert("abc", 321)', res, 123);

        res = t.len();
        check('len()', res, 1);

        res  = t.get('abc');
        check('get("abc")', res, 321);

        res = t.remove('bad');
        check('remove("bad")', res, null )

        res  = t.remove('abc');
        check('remove("abc")', res, 321);

        res = t.len();
        check('len()', res, 0);

        res  = t.remove('abc');
        check('remove("abc")', res, null);

        res = t.len();
        check('len()', res, 0);

        res = t.insert('abc', 123);
        check('insert("abc", 123)', res, null);

        res = t.clear();
        check('clear()', res, undefined);

        res = t.len();
        check('len()', res, 0);

        let add = (function () {
            let counter = 0;
            return function (v) {
                console.log(v)
                counter += v;
                return counter
            }
        })();

        res = t.insert('a', 1);
        res = t.insert('b', 2);
        res = t.insert('c', 3);

        res = t.visit_values(add);
        check('visit_values(add)', res, undefined);

        res = add(0);
        check('add(0)', res, 6);

        let v = [];
        let push_to_v = function (n) { v.push(n); };

        res = t.visit_values(push_to_v);
        check('visit_values(push_to_v)', res, undefined);
        check('v', v, '1,2,3');

        let array = [];
        let push_kv_to_v = function (key, value) { array.push(`${key}=${value}`); };
        res = t.visit_keys_and_values(push_kv_to_v);
        check('visit_values(push_kv_to_v)', res, undefined);
        check('array', array, ["a=1","b=2","c=3"]);


        array = [];
        let pick_keys_and_values = function (getNodeKey, getNodeValue, getNextNode, getNextBackNode, done) {



            array.push(`${key}=${value}`);

        };
        res = t.visit_keys_and_values(push_kv_to_v);
        check('visit_values(push_kv_to_v)', res, undefined);
        check('array', array, ["a=1","b=2","c=3"]);

    })
    .catch(console.error);
