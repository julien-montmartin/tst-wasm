#!/bin/sh

withOpt=false
withGz=false
withGitIgnore=false

root=${HOME}/Dépôts/ternary-tree-wasm

if [ ! -d "${root}" ] ; then

   root=${PWD}
fi

echo "Working in ${root}"

pkg=${root}/pkg
pkg_wasm=${pkg}/ternary_tree_wasm_bg.wasm
tmp_wasm=/tmp/ternary_tree_wasm_bg.wasm
pkg_wasm_gz=${pkg}/ternary_tree_wasm_bg.wasm.gz
docs=${root}/docs


#Clean

rm -Rf "${pkg}"


#Build a release package

wasm-pack build --target web --release "${root}"


if ${withOpt} ; then

#Try to optimize generated wasm with wasm-opt. This is unexpected, but
#optimizing for speed also produce the smaller wasm file...
# - 61K ternary_tree_wasm_bg.wasm
# - 51K ternary_tree_wasm_bg.wasm.O2
# - 50K ternary_tree_wasm_bg.wasm.O4
# - 50K ternary_tree_wasm_bg.wasm.Os
# - 50K ternary_tree_wasm_bg.wasm.Oz

mv "${pkg_wasm}" "${tmp_wasm}"
wasm-opt -O4 -o "${pkg_wasm}" "${tmp_wasm}"

fi


if ${withGz} ; then

	#Compress optimized output with gzip
	# - 19K ternary_tree_wasm_bg.wasm.gz

	gzip --best "${pkg_wasm}"
	ls -lh "${pkg_wasm_gz}"


	#Patch ternary_tree_wasm.js to import ternary_tree_wasm_bg.wasm.gz
	#diff -u ./pkg/ternary_tree_wasm.js.org ./pkg/ternary_tree_wasm.js

	patch -l -d "${root}" -p0 --forward <<EOF
--- ./pkg/ternary_tree_wasm.js.org	2020-01-04 11:00:07.251082289 +0100
+++ ./pkg/ternary_tree_wasm.js	2020-01-04 11:01:31.689181374 +0100
@@ -1,4 +1,4 @@
-import * as wasm from './ternary_tree_wasm_bg.wasm';
+import * as wasm from './ternary_tree_wasm_bg.wasm.gz';

 const heap = new Array(32);

@@ -292,4 +292,3 @@
 export const __wbindgen_throw = function(arg0, arg1) {
     throw new Error(getStringFromWasm0(arg0, arg1));
 };
-
EOF


	#Patch package.json to reference ternary_tree_wasm_bg.wasm.gz
	#diff -u ./pkg/package.json.org ./pkg/package.json

	patch -l -d "${root}" -p0 --forward <<EOF
--- ./pkg/package.json.org	2020-01-04 11:00:18.554828858 +0100
+++ ./pkg/package.json	2020-01-04 11:00:44.418247759 +0100
@@ -11,11 +11,11 @@
     "url": "https://github.com/julien-montmartin/ternary-tree-wasm"
   },
   "files": [
-    "ternary_tree_wasm_bg.wasm",
+    "ternary_tree_wasm_bg.wasm.gz",
     "ternary_tree_wasm.js",
     "ternary_tree_wasm.d.ts"
   ],
   "module": "ternary_tree_wasm.js",
   "types": "ternary_tree_wasm.d.ts",
   "sideEffects": "false"
-}
\ Pas de fin de ligne à la fin du fichier
+}
EOF

fi


if ! ${withGitIgnore} ; then

	# Remove .gitignore
	rm -f "${pkg}"/.gitignore

fi


# Prepare docs for GitHub page

cp -R "${pkg}" "${docs}"

emacs --batch -l "${HOME}"/.emacs "${docs}"/doc.org -f org-html-export-to-html


#Show package files

echo "------------------------------------------------------------"
tree -h "${pkg}"
